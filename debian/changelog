pytest-openfiles (0.6.0-1) unstable; urgency=medium

  * Make d/watch url more flexible
  * New upstream version 0.6.0
  * Push Standards-Version to 4.7.0. No changes needed

 -- Ole Streicher <olebole@debian.org>  Thu, 06 Jun 2024 21:06:17 +0200

pytest-openfiles (0.5.0-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster
  * Update standards version to 4.6.0, no changes needed.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 06 Dec 2022 13:52:36 +0000

pytest-openfiles (0.5.0-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Ole Streicher ]
  * Remove version.py (regenerated by scm) (Closes: #978269)
  * Push Standards-Version to 4.5.1. No changes needed
  * Push dh-compat to 13
  * Add "Rules-Requires-Root: no" to d/control

 -- Ole Streicher <olebole@debian.org>  Sun, 27 Dec 2020 12:10:56 +0100

pytest-openfiles (0.5.0-1) unstable; urgency=low

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Ole Streicher ]
  * New upstream version 0.5.0
  * Push Standards-Version to 4.5.0. No changes needed.
  * Add setuptools-scm build dep

 -- Ole Streicher <olebole@debian.org>  Sun, 19 Apr 2020 17:13:48 +0200

pytest-openfiles (0.4.0-1) unstable; urgency=low

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.0.

  [ Ole Streicher ]
  * New upstream version 0.4.0
  * Push compat to 12
  * Add d/gitlab-ci.yml for salsa

 -- Ole Streicher <olebole@debian.org>  Thu, 25 Jul 2019 00:28:22 +0200

pytest-openfiles (0.3.2-1) unstable; urgency=low

  * New upstream version 0.3.2
  * Push Standards-Version to 4.3.0. No changes needed

 -- Ole Streicher <olebole@debian.org>  Tue, 08 Jan 2019 09:55:16 +0100

pytest-openfiles (0.3.1-2) unstable; urgency=medium

  * Add CI tests
  * Fix maintainer (DPMT) email address (Closes: #914718)
  * Move VCS to DPMT

 -- Ole Streicher <olebole@debian.org>  Tue, 27 Nov 2018 16:40:21 +0100

pytest-openfiles (0.3.1-1) unstable; urgency=low

  * New upstream version 0.3.1
  * Push Standards-Version to 4.2.1. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Mon, 26 Nov 2018 17:23:58 +0100

pytest-openfiles (0.3.0-2) unstable; urgency=low

  * Remove obsolete CI test files (Closes: #896493)

 -- Ole Streicher <olebole@debian.org>  Sat, 21 Apr 2018 20:22:20 +0200

pytest-openfiles (0.3.0-1) unstable; urgency=low

  * New upstream version 0.3.0
  * Push Standards-Version to 4.1.4. No changes needed.

 -- Ole Streicher <olebole@debian.org>  Sat, 21 Apr 2018 10:53:56 +0200

pytest-openfiles (0.2.0-2) unstable; urgency=low

  * Set Arch:all instead of Arch:any

 -- Ole Streicher <olebole@debian.org>  Sun, 04 Feb 2018 10:08:00 +0100

pytest-openfiles (0.2.0-1) unstable; urgency=low

  * Initial release. (Closes: #887023)

 -- Ole Streicher <olebole@debian.org>  Tue, 16 Jan 2018 17:05:01 +0100
