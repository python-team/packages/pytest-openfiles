.gitignore
CHANGES.rst
LICENSE.rst
MANIFEST.in
README.rst
pyproject.toml
setup.cfg
setup.py
tox.ini
.github/workflows/ci_workflows.yml
.github/workflows/publish.yml
pytest_openfiles/__init__.py
pytest_openfiles/plugin.py
pytest_openfiles/version.py
pytest_openfiles.egg-info/PKG-INFO
pytest_openfiles.egg-info/SOURCES.txt
pytest_openfiles.egg-info/dependency_links.txt
pytest_openfiles.egg-info/entry_points.txt
pytest_openfiles.egg-info/not-zip-safe
pytest_openfiles.egg-info/requires.txt
pytest_openfiles.egg-info/top_level.txt
tests/__init__.py
tests/test_open_file_detection.py
tests/data/open_file_detection.txt